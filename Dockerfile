FROM node:10-alpine

RUN npm install yarn -g

WORKDIR /app
COPY package.json yarn.lock /app/
RUN yarn
COPY . /app

RUN yarn run build

EXPOSE 3000

CMD yarn run serve
