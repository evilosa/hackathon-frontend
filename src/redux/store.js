import { createBrowserHistory } from 'history'
import { applyMiddleware, compose, createStore } from 'redux'
import { routerMiddleware } from 'connected-react-router'
import thunk from 'redux-thunk'
import createRootReducer from './createRootReducer'

export const history = createBrowserHistory()

const configureStore = () => {
  let store

  if (window.__REDUX_DEVTOOLS_EXTENSION__) {
    store = createStore(
      createRootReducer(history),
      compose(
        applyMiddleware(routerMiddleware(history)),
        applyMiddleware(thunk),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
      )
    )
  } else {
    store = createStore(
      createRootReducer(history),
      compose(
        applyMiddleware(routerMiddleware(history)),
        applyMiddleware(thunk)
      )
    )
  }

  return store
}

export default configureStore
