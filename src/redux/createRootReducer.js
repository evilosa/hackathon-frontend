import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import mainPage from '../features/MainPage/reducer'
import tradeReducer from '../features/tradeFeature/reducer'

export default history => combineReducers({
  router: connectRouter(history),
  mainPage,
  trades: tradeReducer
})
