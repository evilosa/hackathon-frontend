import React from 'react'
import { Link } from 'react-router-dom'
import Header from '../../components/Header'
import styles from './MainPage.module.sass'
import Footer from '../../components/Footer'
import SearchLine from '../../components/SearchLine'

const MainPage = () => {
  return (
    <div className={styles.wrapper}>
      <Header></Header>
      <div className={styles.main}>
        <div className={styles.search}>
          <SearchLine />
        </div>
        <Link to="/advance-search" className={styles.advancedSearch}>расширенный поиск</Link>
        <button className={styles.searchBtn}>Найти</button>
      </div>
      <Footer></Footer>
    </div>
  )
}

export default MainPage
