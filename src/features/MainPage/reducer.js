import * as t from './actionTypes'

const initialState = {
  isLoading: false,
  error: ''
}

const mainPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case t.RESET_SEARCH:
      return {
        isLoading: false,
        error: ''
      }

    default:
      return state
  }
}

export default mainPageReducer
