import { createSelector } from 'reselect'

export const getMainPageState = store => store.mainPage

export const isLoadingSelector = createSelector(
  [getMainPageState],
  state => state.isLoading
)

export const errorSelector = createSelector(
  [getMainPageState],
  state => state.error
)
