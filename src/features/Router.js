import React from 'react'
import { Route, Switch } from 'react-router-dom'
import AboutPage from './AboutPage'
import MainPage from './MainPage'
import ResultsPage from './ResultsPage'
import AdvancedSearch from './AdvancedSearch'
import tradeFeature from '../features/tradeFeature'

const Router = () => {
  return (
    <React.Fragment>
      <Switch>
        <Route path="/" exact component={MainPage} />
        <Route path="/about" component={AboutPage} />
        <Route path="/results" component={ResultsPage} />
        <Route path="/advanced-search" component={AdvancedSearch} />
        <Route path="/trades/:tradeId/step1" component={tradeFeature.containers.TradeDetailsContainer} />
        <Route path="/trades" component={tradeFeature.containers.TradesListContainer} />
      </Switch>
    </React.Fragment>
  )
}

export default Router
