import { createSelector } from 'reselect'

export const getFeatureState = store => store.trades

export const tradesSelector = createSelector(
  [getFeatureState],
  state => state.get('trades')
)

export const getTrade = (state, { match: { params: { tradeId } } }) => (
  getFeatureState(state).getIn(['trades', tradeId])
)

export const tradeSelector = createSelector(
  [getTrade],
  trade => trade
)

export const isLoadingSelector = createSelector(
  [getFeatureState],
  state => state.get('isLoading')
)

export const errorSelector = createSelector(
  [getFeatureState],
  state => state.get('error')
)
