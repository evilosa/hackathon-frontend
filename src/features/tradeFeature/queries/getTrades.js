import gql from 'graphql-tag'

export const GET_TRADES = gql`
  query {
    trades {
      id
      title
      description
      offers {
        id
        sum
        company {
          id
          taxNumber
          name
          fullName
          description
          isBlocked
        }
      }
    }
  }
`
