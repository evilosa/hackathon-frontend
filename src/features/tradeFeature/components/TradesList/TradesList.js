// @flow
import * as React from 'react'
import HeaderZakupki from '../HeaderZakupki'
import Spinner from '../../../../components/Spinner'

type TProps = {
  history: any,
  trades: any,
  isLoading: boolean,
  error: String,
  fetchTrades: () => Promise<void>,
}

class TradesList extends React.Component<TProps> {
  componentDidMount () {
    this.props.fetchTrades()
  }

  render () {
    const { isLoading, error, trades, history } = this.props

    if (isLoading) return <Spinner />
    if (error) return <div>{error}</div>

    return <div>
      <HeaderZakupki />
      {trades.toArray().map(([key, trade]) => {
        const id = trade.get('id')
        return <div key={id} onClick={() => history.push(`/trades/${id || 'new'}/step1`)}>{trade.get('title')}</div>
      })}
    </div>
  }
}

export default TradesList
