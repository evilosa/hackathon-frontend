// @flow
import React from 'react'
import styles from './Company.module.sass'
import danger from './img/danger.svg'
import redDanger from './img/red-danger.svg'
import suspicious from './img/suspicious.svg'
import perfect from './img/perfect.svg'

type TProps = {
  name: string,
  price: number,
  selected: boolean,
  rating: string,
  green: boolean,
  info: []
}

const Company = ({ name, price, selected, rating, green, info = [] }: TProps) => {
  let companyClassName = [styles.company]
  if (selected === true) {
    companyClassName.push(styles.selected)
  }
  if (selected === false) {
    companyClassName.push(styles.notSelected)
  }
  if (rating === 'danger') {
    companyClassName.push(styles.danger)
  }
  if (rating === 'suspicious') {
    companyClassName.push(styles.suspicious)
  }
  if (rating === 'perfect') {
    companyClassName.push(styles.perfect)
  }
  if (green) {
    companyClassName.push(styles.green)
  }

  return (
    <div className={ companyClassName.join(' ') }>
      <div className={styles.name}>
        { name }
        { rating === 'danger' ? <img src={danger} className={styles.danger} alt="danger" /> : null }
        { rating === 'suspicious' ? <img src={suspicious} className={styles.suspicious} alt="suspicious" /> : null }
        { rating === 'perfect' ? <img src={perfect} className={styles.perfect} alt="perfect" /> : null }
      </div>
      <div className={styles.price}>{ price }</div>
      {
        rating === 'danger' ? (
          <div className={styles.info}>
            <h3>Поставщик не соответствует требованиям к участию в торгах</h3>
            {
              info.map((elem, index) => (
                <div key={index}>
                  <img src={redDanger} alt={'red danger'} />
                  {elem}
                </div>
              ))
            }
          </div>
        ) : null
      }
      {
        rating === 'suspicious' ? (
          <div className={styles.info}>
            <h3>Поставщик явялется подозрительным</h3>
            {
              info.map((elem, index) => (
                <div key={index}>
                  <img src={suspicious} alt={'yellow danger'} />
                  {elem}
                </div>
              ))
            }
          </div>
        ) : null
      }
    </div>
  )
}

export default Company
