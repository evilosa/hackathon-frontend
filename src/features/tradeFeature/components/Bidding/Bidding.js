import React from 'react'
import styles from './Bidding.module.sass'
import Company from '../Company'

class Bidding extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      step: 1,
      providers: [
        {
          name: 'Поставщик 1',
          price: 139700,
          selected: true,
          rating: 'danger',
          info: [
            'У владельца данной компании имеется судимость по статье 159 УК РФ',
            'Компания ликвидируется',
            'Имеется налоговая задолженность'
          ]
        },
        {
          name: 'Поставщик 2',
          price: 143860,
          selected: true,
          rating: 'perfect'
        },
        {
          name: 'Поставщик 3',
          price: 145850,
          selected: true,
          rating: 'danger',
          info: [
            'ОКВЭД не соответсвует',
            'По данному адресу зареистрировано ещё три компании на одно юр.лицо'
          ]
        },
        {
          name: 'Поставщик 4',
          price: 148000,
          selected: true,
          rating: 'suspicious',
          info: [
            'Мать Поставщика 4 является учредителем компаний Поставщика 8 и Поставщика 12 из аукционна'
          ]
        },
        {
          name: 'Поставщик 5',
          price: 150000,
          selected: true,
          rating: 'perfect'
        },
        {
          name: 'Поставщик 6',
          price: 153200,
          selected: false
        },
        {
          name: 'Поставщик 1',
          price: 139700,
          selected: false
        },
        {
          name: 'Поставщик 2',
          price: 143860,
          selected: false
        },
        {
          name: 'Поставщик 3',
          price: 145850,
          selected: false
        },
        {
          name: 'Поставщик 4',
          price: 148000,
          selected: false
        },
        {
          name: 'Поставщик 5',
          price: 150000,
          selected: false
        },
        {
          name: 'Поставщик 6',
          price: 153200,
          selected: false
        }
      ]
    }
  }
  render () {
    return (
      <div>
        {
          this.state.step <= 2 ? <h1>Подведение итогов электронных торгов</h1> : <h1>Рассмотрение вторых частей заявок</h1>
        }
        {
          this.state.step < 5 ? (
            <button onClick={ () => { this.setState({ step: this.state.step + 1 }) } } className={styles.next}>Дальше</button>
          ) : null
        }
        <div className={styles.wrapper}>
          <div className={styles.companies}>
            {
              this.state.providers.map((elem, index) => {
                let company = null
                switch (this.state.step) {
                  case 1:
                    company = <Company name={elem.name} price={elem.price} key={index} />
                    break
                  case 2:
                    company = <Company name={elem.name} price={elem.price} selected={elem.selected} key={index} />
                    break
                  case 3:
                    if (elem.selected) {
                      company = <Company name={elem.name} price={elem.price} rating={elem.rating} info={elem.info} />
                    }
                    break
                  case 4:
                    if (elem.selected && elem.rating !== 'danger') {
                      company = <Company name={elem.name} price={elem.price} rating={elem.rating} info={elem.info} />
                    }
                    break
                  case 5:
                    if (elem.selected) {
                      company = <Company name={elem.name} price={elem.price} selected={false} green={index === 0} />
                    }
                    break
                  default:
                    company = null
                }
                return company
              })
            }
          </div>
        </div>
        {
          this.state.step < 5 ? (
            <button onClick={ () => { this.setState({ step: this.state.step + 1 }) } } className={styles.next}>Дальше</button>
          ) : null
        }
      </div>
    )
  }
}

export default Bidding
