import React from 'react'
import styles from './HeaderZakupki.module.sass'
import leftPart from './img/leftPart.png'
import middlePart from './img/middlePart.png'
import rightPart from './img/rightPart.png'

const HeaderZakupki = () => (
  <div className={styles.header}>
    <img src={leftPart} alt={'left part'} />
    <img src={middlePart} alt={'middle part'} />
    <img src={rightPart} alt={'right part'} />
  </div>
)

export default HeaderZakupki
