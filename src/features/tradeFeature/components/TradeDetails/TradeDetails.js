// @flow
import * as React from 'react'
import Bidding from '../Bidding'
import HeaderZakupki from '../HeaderZakupki'

type TProps = {
  trade: any,
  fetchTrades: () => Promise<void>
}

class TradeDetails extends React.Component<TProps> {
  componentDidMount () {
    if (!this.props.trade) {
      this.props.fetchTrades()
    }
  }

  render () {
    return (
      <React.Fragment>
        <HeaderZakupki />
        <Bidding />
        <div style={{ height: `50px` }} />
      </React.Fragment>
    )
  }
}

export default TradeDetails
