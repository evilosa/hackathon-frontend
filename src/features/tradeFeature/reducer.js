import * as t from './actionTypes'
import { fromJS } from 'immutable'

const initialState = fromJS({
  trades: {},
  isLoading: false,
  error: ''
})

const tradesReducer = (state = initialState, action) => {
  switch (action.type) {
    case t.FETCH_TRADES:
      if (action.meta) {
        if (action.meta.done) {
          return state
            .set('trades', fromJS(action.payload))
            .set('isLoading', false)
            .remove('error')
        }

        if (action.meta.error) {
          return state
            .set('isLoading', false)
            .set('error', action.payload)
        }

        throw new Error(`You provide wrong action to reducer - ${JSON.stringify(action)}`)
      } else {
        return state.set('isLoading', true)
      }

    default:
      return state
  }
}

export default tradesReducer
