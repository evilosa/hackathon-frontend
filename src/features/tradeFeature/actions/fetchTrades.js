import { FETCH_TRADES } from '../actionTypes'
import { GET_TRADES } from '../queries'
import backendGraphQL from '../../../apolloClient'

export const fetchTrades = (props) => async dispatch => {
  dispatch({
    type: FETCH_TRADES
  })

  try {
    const response = await backendGraphQL.query({
      query: GET_TRADES,
      fetchPolicy: 'network-only'
    })

    if (response) {
      const { data: { trades } } = response
      if (trades) {
        dispatch({
          type: FETCH_TRADES,
          payload: trades.reduce((accum, trade) => {
            accum[trade.id] = trade
            return accum
          }, {}),
          meta: { done: true }
        })
      }
    }
  } catch (err) {
    dispatch({
      type: FETCH_TRADES,
      payload: err.message,
      meta: { error: true }
    })
  }
}
