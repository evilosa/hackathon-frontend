import { connect } from 'react-redux'
import { fetchTrades } from '../../actions'
import { tradesSelector, isLoadingSelector, errorSelector } from '../../selectors'

import TradesList from '../../components/TradesList'

const mapStateToProps = (state, props) => ({
  trades: tradesSelector(state),
  isLoading: isLoadingSelector(state),
  error: errorSelector(state)
})

const mapDispatchToProps = {
  fetchTrades
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TradesList)
