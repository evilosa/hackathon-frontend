import { connect } from 'react-redux'
import { fetchTrades } from '../../actions'
import { tradeSelector, isLoadingSelector, errorSelector } from '../../selectors'

import TradeDetails from '../../components/TradeDetails'

const mapStateToProps = (state, props) => ({
  trade: tradeSelector(state, props),
  isLoading: isLoadingSelector(state),
  error: errorSelector(state)
})

const mapDispatchToProps = {
  fetchTrades
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TradeDetails)
