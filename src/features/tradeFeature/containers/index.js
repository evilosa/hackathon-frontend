import TradeDetailsContainer from './TradeDetailsContainer'
import TradesListContainer from './TradesListContainer'

export default {
  TradeDetailsContainer,
  TradesListContainer
}
