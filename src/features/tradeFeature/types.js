// @flow

export type TCompany = {
  id: string;
  taxNumber: string;
  name: string;
  fullName: string;
  description: string;
  isBlocked: boolean;
}

export type TOffer = {
  id: string;
  sum: number;
  company: TCompany;
}

export type TTrade = {
  id: string;
  title: string;
  description: string;
  offers: Array<TOffer>
}
