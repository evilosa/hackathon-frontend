import React from 'react'
import Results from '../../components/Results'
import Layout from '../../components/Layout'

class ResultsPage extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      results:
        [
          {
            title: 'Ремонт дороги в с. Старотураево по улице Восточная муниципального района Ермекеевский район Республики Башкортостан',
            url: 'https://www.roseltorg.ru/procedure/COM21061900020',
            price: '469 517,00'
          },
          {
            title: 'Выполнение работ по текущему ремонту внутрипоселенческих автомобильных дорог на территории сельского поселения "Село Ачан" Амурского муниципального района Хабаровского края в 2019 году.',
            url: 'https://www.roseltorg.ru/procedure/0130300009719000252',
            price: '432 460,00'
          },
          {
            title: 'Капитальный ремонт дворовых территорий МО "Томаринский городской округ" с. Красногорск, ул. Победы 9',
            url: 'https://www.roseltorg.ru/procedure/COM21061900017',
            price: '5 852 370,00'
          },
          {
            title: 'Выполнение работ по текущему ремонту в ГБОУ РШИ №32',
            url: 'https://www.roseltorg.ru/procedure/COM21061900015',
            price: '2 960 576,34'
          },
          {
            title: 'Ремонт дороги в с. Старотураево по улице Восточная муниципального района Ермекеевский район Республики Башкортостан',
            url: 'https://www.roseltorg.ru/procedure/COM21061900038',
            price: '469 517,00'
          }
        ]
    }
  }
  render () {
    return (
      <Layout>
        <Results results={this.state.results} />
      </Layout>
    )
  }
}

export default ResultsPage
