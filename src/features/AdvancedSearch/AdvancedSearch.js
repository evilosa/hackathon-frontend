import React from 'react'
import Layout from '../../components/Layout'
import styles from './AdvancedSearch.module.sass'
import InputRange from 'react-input-range'

const inputRangeClassNames = {
  inputRange: styles.inputRange,
  labelContainer: styles.labelContainer,
  maxLabel: styles.maxLabel,
  minLabel: styles.minLabel,
  valueLabel: styles.valueLabel,
  track: styles.track,
  slider: styles.slider,
  sliderContainer: styles.sliderContainer
}

class AdvancedSearch extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      method: 'аукцион',
      priceValue: 2500000,
      periodValue: 15
    }
  }
  changeMethod = (evt) => {
    this.setState({ method: evt.target.value })
  }
  render () {
    return (
      <Layout>
        <div className={styles.wrapper}>
          <div className={styles.searchLine}>
            <div className={styles.label}>Расширенный поиск</div>
            <div className={styles.input}>
              <input type="text" placeholder="Введите ключевые слова" />
            </div>
          </div>
          <div className={styles.method}>
            <label htmlFor="method-select" className={styles.label}>Способ закупок:</label>
            <select id="method-select" onChange={this.changeMethod} value={this.state.method}>
              <option>аукцион</option>
              <option>конкурс</option>
              <option>запрос котировок</option>
            </select>
          </div>
          {
            this.state.method === 'аукцион' ? (
              <div className={styles.auction}>
                <div>
                  <input type="checkbox" id="opened" />
                  <label htmlFor="opened">открытый</label>
                </div>
                <div>
                  <input type="checkbox" id="closed" />
                  <label htmlFor="closed">закрытый</label>
                </div>
              </div>
            ) : null
          }
          {
            this.state.method === 'конкурс' ? (
              <div className={styles.competition}>
                <div>
                  <input type="checkbox" id="opened" />
                  <label htmlFor="opened">открытый</label>
                </div>
                <div>
                  <input type="checkbox" id="limited" />
                  <label htmlFor="limited">с ограниченным участием</label>
                </div>
                <div>
                  <input type="checkbox" id="two-staged" />
                  <label htmlFor="two-staged">двухэтапный</label>
                </div>
                <div>
                  <input type="checkbox" id="closed" />
                  <label htmlFor="closed">закрытый</label>
                </div>
                <div>
                  <input type="checkbox" id="closed-limited" />
                  <label htmlFor="closed-limited">закрытый с ограниченным участием</label>
                </div>
                <div>
                  <input type="checkbox" id="closed-two-staged" />
                  <label htmlFor="closed-two-staged">закрытый двухэтапный</label>
                </div>
              </div>
            ) : null
          }
          <div className={styles.contractPrice}>
            <div className={styles.label}>Стоимость контракта:</div>
            <InputRange
              classNames={inputRangeClassNames}
              maxValue={5000000}
              minValue={0}
              step={10000}
              value={this.state.priceValue}
              onChange={value => this.setState({ priceValue: value })}
              name={'Стоимость контракта'}
            />
          </div>
          <div className={styles.checkboxes}>
            <div>
              <input type="checkbox" id="app-security" />
              <label htmlFor="app-security">Обеспечение заявки</label>
            </div>
            <div>
              <input type="checkbox" id="contract-security" />
              <label htmlFor="contract-security">Обеспечение контракта</label>
            </div>
          </div>
          <div className={styles.contractPeriod}>
            <div className={styles.label}>Время до конца приёма заявок:</div>
            <InputRange
              classNames={inputRangeClassNames}
              maxValue={30}
              minValue={0}
              step={1}
              value={this.state.periodValue}
              onChange={value => this.setState({ periodValue: value })}
              name={'Стоимость контракта'}
            />
          </div>
          <button className={styles.searchBtn}>
            Найти
          </button>
        </div>
      </Layout>
    )
  }
}

export default AdvancedSearch
