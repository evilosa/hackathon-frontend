// @flow
import React from 'react'
import styles from './Results.module.sass'
import SearchLine from '../../components/SearchLine'

type TProps = {
  results: Array<{
    title: string,
    price: string,
    url: string
  }>
}

const Results = ({ results }: TProps) => (
  <div>
    <div className={styles.searchContainer}>
      <h1 className={styles.resultsLabel}>Найдено 5 конкурсов</h1>
      <SearchLine/>
    </div>
    <div className={styles.resultsContainer}>
      {
        results.map((elem, index) => (
          <div className={styles.result} key={index}>
            <div className={styles.title}>{elem.title}</div>
            <a href={elem.url} className={styles.more}>Подробнее</a>
            <div className={styles.price}>{elem.price}</div>
          </div>
        ))
      }
    </div>
  </div>
)

export default Results
