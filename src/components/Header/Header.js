import React from 'react'
import styles from './Header.module.sass'
import blueLogo from './img/blue-logo.png'

const Header = () => {
  return (
    <div className={styles.header}>
      <div className={styles.logo}>
        <img src={blueLogo} alt="НайтиТорги.РФ логотип"/>
      </div>
      <div className={styles.region}>
        <span>Ваш регоин: </span>
        <span className={styles.current}>Краснодарский край</span>
      </div>
    </div>
  )
}

export default Header
