import React from 'react'
import styles from './Spinner.module.sass'
import SpinnerImg from './img/spinner.gif'

const Spiner = () => (
  <div className={styles.container}>
    <img src={SpinnerImg} alt="download" className={styles.spiner}/>
  </div>
)

export default Spiner
