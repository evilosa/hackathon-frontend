import React from 'react'
import styles from './Footer.module.sass'

const Footer = () => (
  <div className={styles.footer}>© АО «НайтиТорги.РФ» 2019</div>
)

export default Footer
