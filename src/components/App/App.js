import React from 'react'
import Router from '../../features/Router'
import './App.css'

const App = () => {
  return (
    <div className="App">
      <Router />
    </div>
  )
}

export default App
