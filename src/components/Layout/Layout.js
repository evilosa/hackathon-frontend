import React from 'react'
import Header from '../Header'
import Footer from '../Footer'

// eslint-disable-next-line react/prop-types
const Layout = ({ children }) => (
  <React.Fragment>
    <Header />
    { children }
    <Footer />
  </React.Fragment>
)

export default Layout
